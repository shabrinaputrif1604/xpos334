﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using xpos334.datamodels;
using xpos334.viewmodels;

namespace xpos334.api.Controllers
{
	[Route("[controller]")]
	[ApiController]
	public class apiCategoryController : ControllerBase
	{
		private readonly XPOS_334Context db;
		private VMResponse res = new VMResponse();
		private int IdUser = 1;

        public apiCategoryController(XPOS_334Context _db)
        {
			db = _db;
        }

		[HttpGet("GetAllData")]
		public List<TblCategory> GetAllData()
		{
			List<TblCategory> data = db.TblCategories.Where(a => a.IsDelete == false).ToList();
			return data;
		}

		[HttpGet("GetDataById/{id}")]
		public TblCategory GetDataById(int id)
		{
			TblCategory result = db.TblCategories.Where(a => a.Id == id).FirstOrDefault();
			return result;
		}

		[HttpGet("CheckCategoryByName/{name}/{id}")]
		public bool CheckName(string name, int id)
		{
			TblCategory data = new TblCategory();
			if(id == 0)
			{
				data = db.TblCategories.Where(a => a.NameCategory == name && a.IsDelete == false).FirstOrDefault();
			}
			else
			{
				data = db.TblCategories.Where(a => a.NameCategory == name && a.IsDelete == false && a.Id != id).FirstOrDefault();
			}

			if(data != null)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

		[HttpPost("Save")]
		public VMResponse Save(TblCategory data)
		{
			data.CreateBy = IdUser;
			data.CreateDate = DateTime.Now;
			data.IsDelete = false;
			data.Description = data.Description ?? "";

			try
			{
				db.Add(data);
				db.SaveChanges();

				res.Message = "Data saved!";
			}
			catch(Exception e)
			{
				res.Success = false;
				res.Message = "Failed to save " + e.Message;
			}
			
			return res;	
		}

		[HttpPut("Edit")]
		public VMResponse Edit(TblCategory data)
		{
			TblCategory dt = db.TblCategories.Where(a => a.Id == data.Id).FirstOrDefault();

			if(dt != null)
			{
				dt.NameCategory = data.NameCategory;
				dt.Description = data.Description;
				dt.UpdateBy = IdUser;
				dt.UpdateDate = DateTime.Now;

				try
				{
					db.Update(dt);
					db.SaveChanges();

					res.Message = "Data updated";
				}catch (Exception e)
				{
					res.Success = false;
					res.Message = "Failed to save " + e.Message;
				}
			}
			else
			{
				res.Success = false;
				res.Message = "Data not found.";
			}

			return res;
		}

		[HttpDelete("Delete/{id}/{createBy}")]
		public VMResponse Delete(int id, int createBy)
		{
			TblCategory dt = db.TblCategories.Where(a => a.Id == id).FirstOrDefault();

			if (dt != null)
			{
				dt.IsDelete = true;
				dt.UpdateBy = createBy;
				dt.UpdateDate = DateTime.Now;

				try
				{
					db.Update(dt);
					db.SaveChanges();

					res.Message = "Data deleted";
				}
				catch (Exception e)
				{
					res.Success = false;
					res.Message = "Failed to save " + e.Message;
				}
			}
			else
			{
				res.Success = false;
				res.Message = "Data not found.";
			}

			return res;
		}
    }
}

﻿using Newtonsoft.Json;
using xpos334.datamodels;
using xpos334.viewmodels;

namespace xpos334.Services
{
    public class ProductService
    {
        private static readonly HttpClient client = new HttpClient();
        private string RouteApi = "";
        VMResponse response = new VMResponse();
        IConfiguration config;

        public ProductService(IConfiguration _config)
        {
            config = _config;
            RouteApi = config["RouteAPI"];
        }

		public async Task<List<VMTblProduct>> GetAllData()
		{
			List<VMTblProduct> data = new List<VMTblProduct>();

			string apiResponse = await client.GetStringAsync(RouteApi + "apiProduct/GetAllData");
			data = JsonConvert.DeserializeObject<List<VMTblProduct>>(apiResponse);

			return data;
		}
	}
}

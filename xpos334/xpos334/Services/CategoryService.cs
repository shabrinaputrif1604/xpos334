﻿using Newtonsoft.Json;
using System.Text;
using xpos334.datamodels;
using xpos334.viewmodels;

namespace xpos334.Services
{
	public class CategoryService
	{
		//get api url here
		private static readonly HttpClient client = new HttpClient();
		private IConfiguration configuration;
		private string RouteApi = "";
		private VMResponse response = new VMResponse();

        public CategoryService(IConfiguration _configuration)
        {
			configuration = _configuration;
			RouteApi = configuration["RouteAPI"];
        }

		public async Task<List<TblCategory>> GetAllData()
		{
			List<TblCategory> data = new List<TblCategory>();

			string apiResponse = await client.GetStringAsync(RouteApi + "apiCategory/GetAllData");
			data = JsonConvert.DeserializeObject<List<TblCategory>>(apiResponse);
			return data;
		}

		public async Task<VMResponse> Create(TblCategory dataParam)
		{
			//proses convert dari object ke string
			string json = JsonConvert.SerializeObject(dataParam);

			//proses mengubah string menjadi json lalu dikirim sebagai request body
			StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

			//proses memanggil API dan mengirimkan body
			var request = await client.PostAsync(RouteApi + "apiCategory/Save", content);

			if(request.IsSuccessStatusCode)
			{
				//proses membaca respon dari API
				var apiResponse = await request.Content.ReadAsStringAsync();

				//proses convert hasil respon dari API ke objek
				response = JsonConvert.DeserializeObject<VMResponse>(apiResponse);
			}
			else
			{
				response.Success = false;
				response.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
			}

			return response;
		}

		public async Task<bool> CheckCategoryByName(string nameCategory, int id)
		{
			string apiResponse = await client.GetStringAsync(RouteApi + $"apiCategory/CheckCategoryByName/{nameCategory}/{id}");
			bool isExist = JsonConvert.DeserializeObject<bool>(apiResponse);

			return isExist;
		}

		public async Task<TblCategory> GetDataById(int id)
		{
			TblCategory data = new TblCategory();
			string apiResponse = await client.GetStringAsync(RouteApi + $"apiCategory/GetDataById/{id}");
			data = JsonConvert.DeserializeObject<TblCategory>(apiResponse);
			return data;
		}

		public async Task<VMResponse> Edit(TblCategory dataParam)
		{
			string json = JsonConvert.SerializeObject(dataParam);

			StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

			var request = await client.PutAsync(RouteApi + "apiCategory/Edit", content);

			if (request.IsSuccessStatusCode)
			{
				var apiResponse = await request.Content.ReadAsStringAsync();

				response = JsonConvert.DeserializeObject<VMResponse>(apiResponse);
			}
			else
			{
				response.Success = false;
				response.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
			}

			return response;
		}

		public async Task<VMResponse> Delete(int id, int createBy)
		{
			var request = await client.DeleteAsync(RouteApi + $"apiCategory/Delete/{id}/{createBy}");

			if (request.IsSuccessStatusCode)
			{
				//proses membaca respon dari API 
				var apiResponse = await request.Content.ReadAsStringAsync();

				response = JsonConvert.DeserializeObject<VMResponse>(apiResponse);
			}
			else
			{
				response.Success = false;
				response.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
			}

			return response;
		}
	}
}

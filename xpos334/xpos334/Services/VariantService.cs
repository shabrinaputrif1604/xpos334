﻿using Newtonsoft.Json;
using System.Text;
using xpos334.datamodels;
using xpos334.viewmodels;

namespace xpos334.Services
{
    public class VariantService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteApi = "";
        private VMResponse response = new VMResponse();

        public VariantService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteApi = configuration["RouteAPI"];
        }

        public async Task<List<VMTblVariant>> GetAllData()
        {
            List<VMTblVariant> data = new List<VMTblVariant> ();

            string apiResponse = await client.GetStringAsync(RouteApi + "apiVariant/GetAllData");
            data = JsonConvert.DeserializeObject<List<VMTblVariant>>(apiResponse);

            return data;
        }

		public async Task<VMTblVariant> GetDataById(int id)
		{
			VMTblVariant data = new VMTblVariant();

			string apiResponse = await client.GetStringAsync(RouteApi + "apiVariant/GetDataById/" + id);
			data = JsonConvert.DeserializeObject<VMTblVariant>(apiResponse);

			return data;
		}

		public async Task<bool> CheckByName(string name, int id, int idCategory)
		{
			string apiResponse = await client.GetStringAsync(RouteApi + $"apiVariant/CheckByName/{name}/{id}/{idCategory}");
			bool isExist = JsonConvert.DeserializeObject<bool>(apiResponse);

			return isExist;
		}

		public async Task<VMResponse> Create(VMTblVariant dataParam)
		{
			string json = JsonConvert.SerializeObject(dataParam);

			StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

			var request = await client.PostAsync(RouteApi + "apiVariant/Save", content);

			if (request.IsSuccessStatusCode)
			{
				var apiResponse = await request.Content.ReadAsStringAsync();

				response = JsonConvert.DeserializeObject<VMResponse>(apiResponse);
			}
			else
			{
				response.Success = false;
				response.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
			}

			return response;
		}

		public async Task<VMResponse> Edit(VMTblVariant dataParam)
		{
			string json = JsonConvert.SerializeObject(dataParam);

			StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

			var request = await client.PutAsync(RouteApi + "apiVariant/Edit", content);

			if (request.IsSuccessStatusCode)
			{
				var apiResponse = await request.Content.ReadAsStringAsync();

				response = JsonConvert.DeserializeObject<VMResponse>(apiResponse);
				Console.WriteLine("Ini adalah response edit service variant " + response);
			}
			else
			{
				response.Success = false;
				response.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
			}

			return response;
		}

		public async Task<VMResponse> Delete(int id)
		{
            var request = await client.DeleteAsync(RouteApi + $"apiVariant/Delete/{id}");

            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari API 
                var apiResponse = await request.Content.ReadAsStringAsync();

                response = JsonConvert.DeserializeObject<VMResponse>(apiResponse);
            }
            else
            {
                response.Success = false;
                response.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return response;
        }
	}
}

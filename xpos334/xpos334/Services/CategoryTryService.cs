﻿using AutoMapper;
using System.Collections.Generic;
using xpos334.datamodels;
using xpos334.viewmodels;

namespace xpos334.Services
{
    public class CategoryTryService
    {
        private readonly XPOS_334Context db;
        VMResponse response = new VMResponse();
        int idUser = 1;

        public CategoryTryService(XPOS_334Context _db)
        {
            db = _db;
        }

        public static IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<TblCategory, VMTblCategory>();
                cfg.CreateMap<VMTblCategory, TblCategory>();
            });

            IMapper mapper = config.CreateMapper();

            return mapper;
        }

        public List<VMTblCategory> GetAllData()
        {
            List<TblCategory> dataModel = db.TblCategories.Where(a => a.IsDelete == false).ToList();

            List<VMTblCategory> dataView = GetMapper().Map<List<VMTblCategory>>(dataModel);

            return dataView;
        }

        public VMResponse Create(VMTblCategory dataView)
        {
            TblCategory dataModel = GetMapper().Map<TblCategory>(dataView);

            dataModel.IsDelete = false;
            dataModel.CreateBy = idUser;
            dataModel.CreateDate = DateTime.Now;

            try
            {
                db.Add(dataModel);
                db.SaveChanges();

                response.Message = "Data success save.";
                response.Entity = dataModel;
            }
            catch(Exception ex)
            {
                response.Success = false;
                response.Message = "Failed saved: " + ex.Message;
                response.Entity = dataView;
			}

            return response;
        }

        public VMTblCategory GetById(int id)
        {
            TblCategory dataModel = db.TblCategories.Find(id);
            VMTblCategory dataView = GetMapper().Map<VMTblCategory>(dataModel);

            return dataView;
        }

        public VMResponse Edit(VMTblCategory dataView)
        {
            TblCategory dataModel = db.TblCategories.Find(dataView.Id);
            dataModel.NameCategory = dataView.NameCategory;
            dataModel.Description = dataView.Description;
            dataModel.UpdateBy = idUser;
            dataModel.UpdateDate = DateTime.Now;

            try
            {
                db.Update(dataModel);
                db.SaveChanges();

                response.Message = "Data saved.";
                response.Entity = GetMapper().Map<VMTblCategory>(dataModel);
            }
            catch(Exception ex)
            {
                response.Success= false;
                response.Message = "Failed to save." + ex.Message;
                response.Entity = GetMapper().Map<VMTblCategory>(dataView);
			}

            return response;
        }

        public VMResponse Delete(VMTblCategory dataView)
        {
			TblCategory dataModel = db.TblCategories.Find(dataView.Id);
            dataModel.IsDelete = true;
			dataModel.UpdateBy = idUser;
			dataModel.UpdateDate = DateTime.Now;

			try
            {
				db.Update(dataModel);
				db.SaveChanges();

				response.Message = "Data deleted.";
                response.Entity = GetMapper().Map<VMTblCategory>(dataModel);
            }
			catch (Exception ex)
            {
				response.Success = false;
				response.Message = "Failed to delete." + ex.Message;
                response.Entity = GetMapper().Map<VMTblCategory>(dataView);
            }
            return response;
		}
	}
}

﻿using AutoMapper;
using xpos334.datamodels;
using xpos334.viewmodels;

namespace xpos334.Services
{
    public class VariantTryService
    {
        private readonly XPOS_334Context db;
        VMResponse response = new VMResponse();
        int idUser = 1;

        public VariantTryService(XPOS_334Context _db)
        {
            db = _db;
        }

        public static IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<TblVariant, VMTblVariant>();
                cfg.CreateMap<VMTblVariant, TblVariant>();
            });

            IMapper mapper = config.CreateMapper();

            return mapper;
        }

        public List<VMTblVariant> GetAllData()
        {
            List<VMTblVariant> dataView = (from v in db.TblVariants
                                           join c in db.TblCategories on v.IdCategory equals c.Id
                                           where v.IsDelete == false
                                           select new VMTblVariant
                                           {
                                               Id = v.Id,
                                               NameVariant = v.NameVariant,
                                               Description = v.Description,
                                               IdCategory = v.IdCategory,
                                               NameCategory = c.NameCategory
                                           }).ToList();

            return dataView;
        }

        public VMTblVariant GetDataById(int id)
        {
			//VMTblVariant dataView = GetMapper().Map<VMTblVariant>(dataModel);
			VMTblVariant dataView = (from v in db.TblVariants
										   join c in db.TblCategories on v.IdCategory equals c.Id
										   where v.IsDelete == false && v.Id == id
										   select new VMTblVariant
										   {
											   Id = v.Id,
											   NameVariant = v.NameVariant,
											   Description = v.Description,
											   IdCategory = v.IdCategory,
                                               NameCategory = c.NameCategory
										   }).FirstOrDefault()!;

			return dataView;
        }

        public VMResponse Create(VMTblVariant dataView)
        {
			//TblVariant dataModel = GetMapper().Map<TblVariant>(dataView); 
			TblVariant dataModel = new TblVariant(); //harus diset namevariant, idcategory, description
			dataModel.NameVariant = dataView.NameVariant;
			dataModel.IdCategory = dataView.IdCategory;
			dataModel.Description = dataView.Description ?? "";
            dataModel.IsDelete = false;
            dataModel.CreateBy = idUser;
            dataModel.CreateDate = DateTime.Now;

            try
            {
                db.Add(dataModel);
                db.SaveChanges();

                response.Message = "Saved";
                response.Entity = dataModel;
			}
            catch(Exception e)
            {
                response.Success = false;
                response.Message = "Failed to save " + e.Message;
                response.Entity = dataView;
            }

            return response;
		}

        public VMResponse Edit(VMTblVariant dataView)
        {
            TblVariant dataModel = db.TblVariants.Find(dataView.Id);
            dataModel.NameVariant = dataView.NameVariant;
            dataModel.IdCategory = dataView.IdCategory;
            dataModel.Description = dataView.Description ?? "";
            dataModel.UpdateDate = DateTime.Now;
            dataModel.UpdateBy = idUser;

            try
            {
                db.Update(dataModel);
                db.SaveChanges();

                response.Message = "Updated";
                response.Entity = dataModel;
            }
            catch(Exception e)
            {
                response.Success = false;
                response.Message = "Failed to update.";
                response.Entity = dataView;
            }

            return response;
		}

        public VMResponse Delete(VMTblVariant dataView)
        {
            TblVariant dataModel = db.TblVariants.Find(dataView.Id);

            dataModel.IsDelete = true;
            dataModel.UpdateBy = idUser;
            dataModel.UpdateDate = DateTime.Now;

			//Console.Write("Ini service, apakah masukkkkk?");


			try
			{
                Console.Write("Try variant, apakah masukkkkk?");
                db.Update(dataModel);
                db.SaveChanges();

                response.Message = "Deleted";
                response.Entity = dataModel;
            }
            catch(Exception ex)
            {
				Console.Write("Catch variant, apakah masukkkkk?");
				response.Success = false;
				response.Message = "Failed" + ex.Message;
				response.Entity = dataView;
			}

            return response;
        }
    }
}

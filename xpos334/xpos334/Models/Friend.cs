﻿namespace xpos334.Models
{
	public class Friend
	{
		//properties
		public int Id { get; set; }
		public string Name { get; set; }
		public string Address { get; set; }
	}
}

using Microsoft.EntityFrameworkCore;
using xpos334.datamodels;
using xpos334.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

//Untuk menjalankan category service dari awal program dijalankan
builder.Services.AddScoped<CategoryService>();

//Untuk menjalankan variant service dari awal program dijalankan
builder.Services.AddScoped<VariantService>();

//Untuk menjalankan variant service dari awal program dijalankan
builder.Services.AddScoped<ProductService>();

// Add connection string
builder.Services.AddDbContext<XPOS_334Context>(option =>
{
	option.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection"));
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
	app.UseExceptionHandler("/Home/Error");
	// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
	app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
		name: "default",
		pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();

﻿using Microsoft.AspNetCore.Mvc;
using xpos334.datamodels;
using xpos334.Services;
using xpos334.viewmodels;

namespace xpos334.Controllers
{
    public class ProductController : Controller
    {
        private CategoryService categoryService;
        private VariantService variantService;
        private ProductService productService;
        private int IdUser = 1;

        public ProductController(CategoryService _categoryService, VariantService _variantService, ProductService _productService)
        {
            categoryService = _categoryService;
            variantService = _variantService;
            productService = _productService;
        }

        public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.PriceSort = sortOrder == "price" ? "price_desc" : "";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            List<VMTblProduct> data = await productService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.NameProduct.ToLower().Contains(searchString.ToLower())
                || a.NameVariant.ToLower().Contains(searchString.ToLower())
                || a.NameCategory.ToLower().Contains(searchString.ToLower())).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.NameProduct).ToList();
                    break;
                case "price_desc":
                    data = data.OrderByDescending(a => a.Price).ToList();
                    break;
                case "price":
                    data = data.OrderBy(a => a.Price).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.NameProduct).ToList();
                    break;
            }
            return View(PaginatedList<VMTblProduct>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));
        }

        public async Task<IActionResult> Create()
        {
            VMTblProduct data = new VMTblProduct();

            List<TblCategory> listCategory = await categoryService.GetAllData();
            ViewBag.ListCategory = listCategory;  
            
            List<VMTblVariant> listVariant = await variantService.GetAllData();
            ViewBag.ListVariant = listVariant;

            return PartialView(data);
        }
    }
}

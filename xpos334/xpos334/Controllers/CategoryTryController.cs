﻿using Microsoft.AspNetCore.Mvc;
using xpos334.datamodels;
using xpos334.Services;
using xpos334.viewmodels;

namespace xpos334.Controllers
{
    public class CategoryTryController : Controller
    {
        private readonly XPOS_334Context db;
        private readonly CategoryTryService categoryTryService;
        public CategoryTryController(XPOS_334Context _db)
        {
            db = _db;
            this.categoryTryService = new CategoryTryService(db);
        }
        public IActionResult Index()
        {
            List<VMTblCategory> dataView = categoryTryService.GetAllData();
            
            return View(dataView);
        }

        public IActionResult Create()
        {
            VMTblCategory dataView = new VMTblCategory();
            return PartialView(dataView);
            //return View(dataView); //pakai layout null
        }

        [HttpPost]
        public IActionResult Create(VMTblCategory dataView)
        {
            VMResponse response = new VMResponse();
            //TblCategory data = new TblCategory();
            //data.NameCategory = dataView.NameCategory;
            //db.TblCategories.Add(data);

            if(ModelState.IsValid)
            {
                response = categoryTryService.Create(dataView);

                if (response.Success)
                {
                    return RedirectToAction("Index");
                }
            }

            response.Entity = dataView;
            return View(response.Entity);
        }

        public IActionResult Edit(int id)
        {
            VMTblCategory dataView = categoryTryService.GetById(id);
            return PartialView(dataView);
        }

        [HttpPost]
        public IActionResult Edit(VMTblCategory dataView)
        {
            VMResponse response = new VMResponse();

            if(ModelState.IsValid)
            {
                response = categoryTryService.Edit(dataView);

                if(response.Success)
                {
                    return RedirectToAction("Index");
                }
            }

            response.Entity = dataView;
            return View(response.Entity);
        }
        public IActionResult Detail(int id)
        {
            VMTblCategory dataView = categoryTryService.GetById(id);
            return PartialView(dataView);
        }

        public IActionResult Delete(int id)
        {
			VMTblCategory dataView = categoryTryService.GetById(id);
            Console.WriteLine("Ini"+dataView);
			return PartialView(dataView);
		}

        [HttpPost]
		public IActionResult Delete(VMTblCategory dataView)
		{
            VMResponse response = categoryTryService.Delete(dataView);

			if (response.Success)
			{
				return RedirectToAction("Index");
			}

			return View(response.Entity);
		}
	}
}

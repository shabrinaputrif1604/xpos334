﻿using Microsoft.AspNetCore.Mvc;

namespace xpos334.Controllers
{
	public class TestController : Controller
	{
		public class Kumpulan
		{
			public int x { get; set; }
			public int y { get; set; }

			public Kumpulan(int x, int y)
			{
				this.x = x; this.y = y;
			}
		}

		public IActionResult Index()
		{
			string x = "Ini viewbag";
			ViewBag.x = x;


			Kumpulan kumpulan = new Kumpulan(1,2);
			Kumpulan kumpulan2 = new Kumpulan(2,2);
			Kumpulan kumpulan3 = new Kumpulan(3,2);
			Kumpulan kumpulan4 = new Kumpulan(4,2);
			Kumpulan kumpulan5 = new Kumpulan(5,2);

			List<Kumpulan> kumkumpulan = new List<Kumpulan>()
			{
				kumpulan, 
				kumpulan2,
				kumpulan3,
				kumpulan4,
				kumpulan5
			};

			return View(kumkumpulan);
		}

		[HttpPost]
		public string Posting()
		{
			return "Hello";
		}
	}
}

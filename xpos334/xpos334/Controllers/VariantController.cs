﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Build.Framework;
using System.Drawing.Printing;
using xpos334.datamodels;
using xpos334.Services;
using xpos334.viewmodels;

namespace xpos334.Controllers
{
    public class VariantController : Controller
    {
        private CategoryService categoryService;
        private VariantService variantService;
        private int IdUser = 1;

        public VariantController(CategoryService _categoryService, VariantService _variantService)
        {
            categoryService = _categoryService;
            variantService = _variantService;
        }
        public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            List<VMTblVariant> data = await variantService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.NameCategory.ToLower().Contains(searchString.ToLower()) 
                || a.NameVariant.ToLower().Contains(searchString.ToLower())
                || a.Description.ToLower().Contains(searchString.ToLower())).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.NameCategory).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.NameCategory).ToList();
                    break;
            }

        return View(PaginatedList<VMTblVariant>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));
        }
		public async Task<JsonResult> CheckNameIsExist(string nameVariant, int id, int idCategory)
		{
			bool isExist = await variantService.CheckByName(nameVariant, id, idCategory);
			return Json(isExist);
		}
        
        public async Task<IActionResult> Create()
        {
            VMTblVariant data = new VMTblVariant();

            //Get all data master category
            List<TblCategory> listCategories = await categoryService.GetAllData();
            ViewBag.ListCategories = listCategories;
            //Console.WriteLine("testttt");
            return PartialView(data);
		}

		[HttpPost]
		public async Task<IActionResult> Create(VMTblVariant dataParam)
		{
			dataParam.CreateBy = IdUser;
			VMResponse response = await variantService.Create(dataParam);

			if (response.Success)
			{
				return Json(new { dataResponse = response });
			}

			return View(dataParam);
		}

		public async Task<IActionResult> Edit(int id)
		{
			VMTblVariant data = await variantService.GetDataById(id);

			//Get all data master category
			List<TblCategory> listCategories = await categoryService.GetAllData();
			ViewBag.ListCategories = listCategories;

			return PartialView(data);
		}

        [HttpPost]
		public async Task<IActionResult> Edit(VMTblVariant dataParam)
		{
			VMResponse response = await variantService.Edit(dataParam);

			if (response.Success)
			{
				return Json(new { dataResponse = response });
			}

			return View(dataParam);
		}

        public async Task<IActionResult> Detail(int id)
        {
            VMTblVariant data = await variantService.GetDataById(id);

            return PartialView(data);
        }

		public async Task<IActionResult> Delete(int id)
		{
			VMTblVariant data = await variantService.GetDataById(id);

			return PartialView(data);
		}

        [HttpPost]
        public async Task<IActionResult> SureDelete(int id)
        {
            VMResponse response = await variantService.Delete(id);

            if (response.Success)
            {
                return Json(new {dataResponse  = response});
            }

            return RedirectToAction("Index");
        }
	}
}
